Rails.application.routes.draw do

  get 'item/list'

  get 'item/show'

  root to: 'home#index'

  get '/send_mail', to: 'home#send_mail'
  get '/contacts', to: 'home#contacts'
  get '/info', to: 'home#info'

  get '/items/:type', to: 'items#list', as: 'items_list'
  resources :items, only: [:show]

  resources :cart
end
