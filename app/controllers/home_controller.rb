class HomeController < ApplicationController
  def index
  end

  def contacts
  end

  def info
  end

  def send_mail
  	if params[:letter]
      Info.send_mail(params[:letter][:name], params[:letter][:email], params[:letter][:body])
      flash[:success] = 'Ваше письмо отправлено! Мы скоро с Вами свяжемся!'
  	  redirect_to root_path
    else
      flash[:danger] = 'Письмо не отправлено! Проверьте правильность указанных данных.'
      render :index
    end
  end

  def validate_letter_params(letter_params)
    letter_params.each do |attribute|
       
    end
  end

  def email?
    /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.match self
  end
end
