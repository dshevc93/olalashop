class Info < ActionMailer::Base
  default from: 'contact_us@o-lyalya.com'

  def send_mail(name, email, body)
    @name, @email, @body = name, email, body

    mail to: 'info@o-lyalya.com', 
         subject: 'Сообщение пользователя o-lyalya.com'
  end
end
