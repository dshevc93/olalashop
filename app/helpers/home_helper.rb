module HomeHelper

  def panel_is_active?(url)
  	if (request.env['REQUEST_PATH'] == url) || (controller == 'cart')
  	  'active'
  	end
  end

end
